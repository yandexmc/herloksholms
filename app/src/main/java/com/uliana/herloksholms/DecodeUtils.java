package com.uliana.herloksholms;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

public class DecodeUtils {

    public static String decodeStream(InputStream inputStream) throws IOException {
        int COUNT_OF_HEADER_BYTES = 43;
        String resultMessage = "";

        byte readBuffer[] = new byte[1000];
        int len;
        int result = 0;
        int exp = 4;
        int countOfZero = 0;

        inputStream.skip(COUNT_OF_HEADER_BYTES);
        do {
            len = inputStream.read(readBuffer);
            for (int i = 0; i < len; i++) {
                //take every second byte
                if (i % 2 == 1) {
                    //check last bit
                    int value = readBuffer[i] & 1;

                    result = result | (value << exp);

                    if (exp == 0) {
                        resultMessage += String.format("%c", result + 'а');
                        countOfZero = result == 0 ? countOfZero + 1 : 0;
                        if (countOfZero >= 3) {
                            break;
                        }
                        result = 0;
                        exp = 4;
                    } else {
                        exp--;
                    }
                }
            }
        } while (len != -1 && countOfZero < 3);

        Log.d(DecodeUtils.class.getSimpleName(), resultMessage);
        return resultMessage;
    }
}
