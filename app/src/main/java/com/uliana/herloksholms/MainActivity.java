package com.uliana.herloksholms;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.tv_secret_message)
    TextView textViewSecretMessage;

    @BindView(R.id.tv_file_name)
    TextView textViewSFileName;

    @BindView(R.id.tv_secret_message_title)
    TextView getTextViewSecretMessage;

    private File selectedFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.btn_encode)
    public void onEncodeClicked() {
        try {
            //decode default file if file not selected
            if (selectedFile == null) {
                InputStream inputStream = this.getAssets().open(getString(R.string.file_example_name));
                showDecodedMessage(DecodeUtils.decodeStream(inputStream));
            } else {
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                showDecodedMessage(DecodeUtils.decodeStream(fileInputStream));
            }
        } catch (IOException e) {
            Log.e(MainActivity.class.getSimpleName(), "Problem with file");
        }
    }

    @OnClick(R.id.btn_open)
    public void onOpenClicked() {
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.extensions = new String[]{getString(R.string.file_extension)};

        FilePickerDialog dialog = new FilePickerDialog(MainActivity.this, properties);
        dialog.setTitle(getString(R.string.dialog_select_file_title));

        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                if (files.length > 0) {
                    selectedFile = new File(files[0]);
                    textViewSFileName.setText(selectedFile.getName());
                }
            }
        });

        dialog.show();
    }

    private void showDecodedMessage(String message) {
        getTextViewSecretMessage.setVisibility(View.VISIBLE);
        textViewSecretMessage.setText(message);
    }

}
