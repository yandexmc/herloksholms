#Задача 4 - HerlokSholms
#### Решение
Цаза - дает понять что шифруется русский алфавит и каким образом это происходит
Шифрование происходит заменой младшего бита в каждых двух байтах на бит шифруемого сообщения, таким образом одна буква шифруется в 10 байтах аудио файла. 
Байты, соответствующие заголовку формата wav в шифровании не участвуют.
Концом шифруемого сообщения считаю 3 нулевых байта, т.к. не существует слова с 3, подряд идущими буквами а.

Не знаю, нужно ли было рассматривать расширенные случаи (например, если шифрование происходило с использованием другого количества байт)

#### Дополнительные библиотеки
1. Butterknife
2. android-filepicker - Для выбора файла с устройства
